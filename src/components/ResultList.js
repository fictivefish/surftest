import React from 'react';
import ListGroupItem from 'react-bootstrap/ListGroupItem';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

export default class ResultList extends React.Component {
	
	// eslint-disable-next-line
	constructor(props) {
		super(props);	
	}

	render() {
		
		let results = [];
		
		this.props.places.map((item, key) => {
			
			let open;
			
			try{
				open = "Opens: " + item.opening_hours.periods[1].open.time;
				open += ", closes: " + item.opening_hours.periods[1].close.time;
			}
			catch(e){
				open='Opening hours unavailable';
			}
			
			
			let place = <ListGroupItem key={key}>
							<Row>
								<Col sm={8}>
									<span className="font-weight-bold">{item.name}</span>
								</Col>
								<Col sm={2}>
									<p>
										<span>Rating: </span>
										<span className="font-weight-bold" >{item.rating}</span>
									</p>
								</Col>
								<Col sm={2}>
									<p>
										<span className="font-italic" >{open}</span>
									</p>
								</Col>
							</Row>
						</ListGroupItem>;
			results.push(place);
			
			return true;
		});
		
		if (results.length > 0) return results;
		
		return <Alert variant="warning">No results found within 1km.</Alert>
		
	}
}