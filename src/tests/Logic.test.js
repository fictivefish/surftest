import React from 'react';
import renderer from 'react-test-renderer';

test('API Key Added as REACT_APP_GAPI_KEY', () => {
	require('dotenv').config();
	/*
	* Test that the Google Places API Key has been 
	* installed in .env as REACT_APP_GAPI_KEY
	*/
	const gApiKey = process.env.REACT_APP_GAPI_KEY;
	expect(gApiKey).toBeTruthy();
});

