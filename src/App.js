import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import ResultList from './components/ResultList';

import './App.css';

import 'bootstrap/dist/css/bootstrap.css';

require('dotenv').config();

const gApiKey = process.env.REACT_APP_GAPI_KEY;

let google;

export default class App extends React.Component {

	constructor(props){
		super(props);
		
		this.state = {
			latitude: 0,
			longitude: 0,
			places: [],
			locationDetected: false,
			error:''
		};
		
		/*
		 * Bind this to the GAPI result setter since it is 
		 * later called inside a callback
		 */
		this.boundHandler = this.handleGAPIResults.bind(this);
		
		// Bindings for input changes 
		this.handleLatitudeChange = this.handleLatitudeChange.bind(this);
		this.handleLongitudeChange = this.handleLongitudeChange.bind(this);
		this.testLocation = this.testLocation.bind(this);
	}
	
	getGoogleMaps() {
		
		// If we haven't already defined the promise, define it
		if (!this.googleMapsPromise) {
			
			this.googleMapsPromise = new Promise((resolve) => {

				// Add a global handler for when the API finishes loading
				window.resolveGoogleMapsPromise = () => {

					// Resolve the promise
					resolve(google);
					// Tidy up
					delete window.resolveGoogleMapsPromise;
				};

				// Load the Google Maps API
				const script = document.createElement("script");
				script.src = `https://maps.googleapis.com/maps/api/js?key=${gApiKey}&libraries=places&callback=resolveGoogleMapsPromise`;
				script.async = true;
				document.body.appendChild(script);
			});
		}

		// Return a promise for the Google Maps API
		return this.googleMapsPromise;
	}

	componentWillMount() {
		// Start Google Maps API loading since we know we'll soon need it
		this.getGoogleMaps();
	}

	componentDidMount() {
		// Get the user's location
		this.getLocation();
	}

	getLocation(){

		// Get the current position of the user
		navigator.geolocation.getCurrentPosition(
			(position) => {
				this.setState(
					(prevState) => ({
						latitude: position.coords.latitude, 
						longitude: position.coords.longitude,
						locationDetected: true
					}), () => {
						this.initMaps();
					}
				);
			},
			(error) => {
				this.setState({ places: error.message });
				this.initMaps();
			},
			{ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
		);
	}
			
	initMaps() {
		// Once the Google Maps API has finished loading, initialize the map
			
		this.getGoogleMaps().then((boundHandler) => {
			const google = window.google;
			let latlng = new google.maps.LatLng(this.state.latitude, this.state.longitude);
			let request = {
				location: latlng,
				radius: '1000',
				//radius: '6000000',
				type: ['lodging'],
				keyword: 'surf',
			};
			const service = new google.maps.places.PlacesService(document.createElement('div'));
			service.nearbySearch(request, this.boundHandler);
		});
	}
		
	handleGAPIResults(results) {
		this.setState({ places: results });
	}
		
	handleLatitudeChange(event) {
		this.setState({latitude: event.target.value, locationDetected: true});
		this.initMaps();
	}
		
	handleLongitudeChange(event) {
		this.setState({longitude: event.target.value, locationDetected: true});
		this.initMaps();
	}
	
	// override the default location with a known match
	testLocation() {
		this.setState({latitude: 43.101184, longitude: -0.162897, locationDetected: true});
		this.initMaps();
	}
			
	locationFound(){
		this.setState({ locationDetected: true });
	}

	render() {
		
		return (
			<Container>
				<Row className="pt-4">
					<Col lg={4}>
						<Alert variant="primary">Manually enter latitude and longitude to override geolocation, or if geolocation was declined.</Alert>
						<Form>
							<Form.Group controlId="formLat">
								<Form.Label>Latitude</Form.Label>
								<Form.Control type="text" placeholder="Latitude" value={this.state.latitude} onChange={this.handleLatitudeChange} />
							</Form.Group>
							<Form.Group controlId="formLng">
								<Form.Label>Longitude</Form.Label>
								<Form.Control type="text" placeholder="Longitude" value={this.state.longitude} onChange={this.handleLongitudeChange} />
							</Form.Group>
						</Form>
						<Button variant="primary" onClick={this.testLocation} >Test with a known location</Button>
					</Col>
					<Col lg={8}>
						{ this.state.locationDetected // check for location service premission/support
							? <ListGroup>
									<ResultList places={this.state.places} />
								</ListGroup>
							: <Alert variant="warning">Manual control only. Please select latitude and longitude.</Alert>
						}
					</Col>
				</Row>
			</Container>
		);
	}
}